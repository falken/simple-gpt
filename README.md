# simple-gpt

A super simple starter web GUI for https://github.com/Mozilla-Ocho/llamafile

<img src="Screenshot_20240501_201721.png">

# Getting started

1. Start llamafile's OpenAI compatible server :
```
./mistral-7b-instruct-v0.2.Q4_0.llamafile --nobrowser
```

You can add `--host` and/or `--port` if you need to 

2. Adjust the host, port and proto params in index.html to match

3. Start a server for these files :
```
python3 -m http.serve
```

Open in browser and start chatting.

Use the big red button to about output without waiting.